\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{uzhthesis}
              [2019/09/10 v0.1
               UZH LaTeX document class]

\LoadClass[a4paper,11pt,twoside,openright]{report}
\usepackage[left=2.5cm,top=3cm,bottom=3cm,right=2.5cm]{geometry}
\usepackage[german,british]{babel}
\usepackage{enumitem}

\makeatletter
\newcommand*{\from}[1]{\def\@from{#1}}
\newcommand*{\committee}[1]{\def\@committee{#1}}
\newcommand*{\member}[1]{#1\\[6mm]}
\newcommand*{\abstractE}[1]{\def\@abstracte{#1}}
\newcommand*{\abstractG}[1]{\def\@abstractg{#1}}
\newcommand*{\declaration}[1]{\def\@declaration{#1}}
\newcommand*{\acknowledgement}[1]{\def\@ackn{#1}}


\def\@emptypage{%
    \hbox{}%
    \thispagestyle{empty}%
    \newpage%
}

\def\cleardoublepage{%
    \clearpage%
    \ifodd\c@page%
        % do nothing
    \else%
        \@emptypage%
    \fi%
}

\renewcommand{\abstractname}{\LARGE Abstract}

\renewcommand\maketitle{
    \pagenumbering{roman}
    \begin{titlepage}
        \begin{center}
            {\Huge\textbf{\@title}}
            \\[0.5cm]
            \noindent\rule{\textwidth}{0.5pt}
            \\[1.1cm]
            {
                \bf
                Dissertation\\[5mm]
                zur\\[5mm]
                Erlangung der naturwissenschaftlichen Doktorw\"{u}rde\\
                (Dr. sc. nat.)\\[5mm]
                vorgelegt der\\[5mm]
                Mathematisch-naturwissenschaftlichen Fakult\"{a}t\\[5mm]
                der\\[5mm]
                Universit\"{a}t Z\"{u}rich\\[5mm]
                von\\[5mm]
            }
            \@author\\[8mm]
            \textbf{aus}\\[8mm]
            \@from\\[13mm]
            \textbf{Promotionskommission}\\[6mm]
            \@committee
            \vspace{6mm}
            \textbf{Z\"{u}rich, \the\year}
        \end{center}
    \end{titlepage}
    \cleardoublepage

    \vspace*{\fill}
    \begin{center}
        {\Large\bfseries Abstract\vspace{-.5em}\vspace{\z@}}
    \end{center}
    \begin{quotation}
        \noindent\@abstracte
    \end{quotation}
    \vspace*{\fill}

    \cleardoublepage
    
    \ifdefined\@abstractg
    \begin{otherlanguage}{german}
        \vspace*{\fill}
        \begin{center}
            {\Large\bfseries Zusammenfassung\vspace{-.5em}\vspace{\z@}}
        \end{center}
    \end{otherlanguage}
    \begin{quotation}
        \noindent\@abstractg
    \end{quotation}
    \vspace*{\fill}
    \fi

    \cleardoublepage
    
    \ifdefined\@ackn
    \vspace*{\fill}
    \begin{center}
        {\Large\bfseries Acknowledgement\vspace{-.5em}\vspace{\z@}}
    \end{center}
    \begin{quotation}
        \noindent\@ackn
    \end{quotation}
    \vspace*{\fill}
    \fi
    \cleardoublepage
    
    \ifdefined\@declaration
    \vspace*{\fill}
    \begin{center}
        {\Large\bfseries Declaration\vspace{-.5em}\vspace{\z@}}
    \end{center}
    \begin{quotation}
        \renewcommand\bibitem[1]\item
        \renewenvironment{thebibliography}[1]{\begin{enumerate}[label={[\arabic*]}]}{\end{enumerate}}%
        \noindent\@declaration
    \end{quotation}
    \vspace*{\fill}
    \fi

    \cleardoublepage

    \tableofcontents
    \cleardoublepage
    \pagestyle{headings}
    \pagenumbering{arabic}
}
\makeatother
