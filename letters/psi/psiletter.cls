\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{psiletter}
              [2019/09/10 v0.1
               PSI LaTeX document class]

\LoadClass[a4paper,11pt]{article}

\newcommand*{\name}[1]{\def\fromname{#1}}
\newcommand*{\address}[1]{\def\fromaddr{#1}}
\newcommand*{\unit}[1]{\def\fromunit{#1}}
\newcommand*{\telephone}[1]{\def\fromphone{#1}}
\newcommand*{\email}[1]{\def\fromemail{#1}}
\newcommand*{\addressfield}[1]{\def\tofield{#1}}
\newcommand*{\signature}[1]{\def\fromsig{#1}}
\newcommand*{\closing}[1]{\def\fromclose{#1}}
\newcommand*{\place}[1]{\def\fromplace{#1}}

\name{}
\address{}
\unit{}
\telephone{}
\email{}
\addressfield{}
\signature{}
\closing{Yours sincerely}
\place{Villigen PSI}

\usepackage[
    a4paper,
    left=4.5cm,right=2cm,
    top=7.0cm,bottom=2cm,
    headheight=5.7cm,
    headsep=0pt,
    textwidth=13cm,
    marginparsep=0pt
    ]{geometry}

\usepackage[sfdefault,lf]{carlito}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[colorlinks=true
,urlcolor=blue
,anchorcolor=blue
,citecolor=blue
,filecolor=blue
,linkcolor=blue
,menucolor=blue
,linktocpage=true
,pdfproducer=medialab
]{hyperref}
\usepackage{fancyhdr}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\setlength{\parindent}{0pt}
\pagestyle{fancy}
\definecolor{textgray}{RGB}{89,89,89}
\definecolor{boxgray}{RGB}{180,180,180}



% the distances are as follows
%   top - top of logo 1.3cm
%   logo height 1.1cm
%   top - text: 4.8cm

\lhead{
    \parbox[t][5.7cm]{9cm}{
        \ifnum\thepage=1
            \includegraphics[width=3.14cm,height=1.1cm]{logo}
            \\[2.3cm]
            \makebox[\textwidth][r]{
                \begin{minipage}[t][2.4cm]{3cm}
                    \footnotesize\raggedleft
                    Paul Scherrer Institut\\
                    Forschungsstrasse 111\\
                    5232 Villigen PSI\\
                    \iflanguage{english}{Switzerland}{Schweiz}\\[1ex]
                    +41 56 310 36 56\\
                    www.psi.ch
                \end{minipage}\hspace{0.6cm}\begin{minipage}[t][2.4cm]{7.2cm}
                    \footnotesize
                    \fromname\\
                    \fromaddr\\
                    \fromunit\\[1ex]
                    \fromphone\\
                    \href{mailto:\fromemail}{\fromemail}
                \end{minipage}\hspace{0.2cm}\begin{minipage}[t][2.4cm]{7cm}
                    \ifx\tofield\empty
                        ~
                    \else
                        \tofield
                    \fi
                \end{minipage}
            }
        \else
            \includegraphics[width=3.14cm,height=1.1cm]{logo}
            \\[1.2cm]
            \makebox[\textwidth][r]{
                \begin{minipage}[t][2.4cm]{3cm}
                    \footnotesize\raggedleft
                    Paul Scherrer Institut\\
                    Forschungsstrasse 111\\
                    5232 Villigen PSI\\
                    \iflanguage{english}{Switzerland}{Schweiz}\\[1ex]
                    +41 56 310 36 56\\
                    www.psi.ch
                \end{minipage}\hspace{0.6cm}\begin{minipage}[t][2.4cm]{14.4cm}
                    \footnotesize
                    \fromplace, \@date\\
                    \iflanguage{english}{page}{Seite} \thepage
                \end{minipage}
            }
        \fi
    }
}
\newenvironment{letter}[2]{{
    \newpage
    \setcounter{page}{1}
    \parbox[t][2.1cm]{15.4cm}{}
    \begin{minipage}[t][0.9cm]{15.4cm}
        \footnotesize
        \fromplace, \@date\par
    \end{minipage}

    \makebox[\textwidth][r]{
        \begin{minipage}[b][1.8cm]{3cm}
            \raggedleft
            \color{boxgray}\rule[-1.4cm]{1.8cm}{1.8cm}
        \end{minipage}\hspace{0.6cm}\begin{minipage}[t][1.8cm]{14.4cm}
            \noindent \color{textgray}\fontfamily{qpl}\selectfont\Large #1
        \end{minipage}
    }

    \vspace{-0.75em}#2
}
\setlength\parskip{1em}
}{
    \fromclose

    \ifx\fromsig\empty
        \fromname
    \else
        \fromsig
    \fi
}
