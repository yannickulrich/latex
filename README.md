# Collection of LaTeX styles

This is a collection of LaTeX templates, style files and classes I
have written over the years. Most likely, they are quite specific to
whatever I wanted to accomplish at the time and probably not very
useful for other people.
