\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{homework}
\LoadClass[a4paper,11pt]{article}

\usepackage{graphicx}
\usepackage{substr}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{xifthen}
\usepackage{framed}
\usepackage{verbatim}

%
% Setup the layout
%

\usepackage[left=2cm,right=2cm,top=2cm,bottom=3cm]{geometry}
\linespread{1.15}
\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}

\pagestyle{empty}

\makeatletter
\newcommand*{\sheetnr}[1]{\def\@sheetnr{#1}}
\newcommand*{\lecturer}[1]{\def\@lecturer{#1}}
\newcommand*{\lecture}[1]{\def\@lecture{#1}}
\newcommand{\sheettype}[2][]{
  \ifthenelse{\equal{#1}{}}{
    \def\@sheettypes{#2s}
  }{
    \def\@sheettypes{#1}
  }
  \def\@sheettype{#2}
}
\newcommand*{\term}[1]{\def\@term{#1}}
\newcommand*{\assistant}[1]{\def\@assistant{#1}}
\newcommand*{\website}[1]{\def\@url{#1}}
\newcommand*{\issued}[1]{\def\@issued{#1}}
\newcommand*{\due}[1]{\def\@due{#1}}
\newcommand*{\logo}[2][2.5cm]{
  \def\@logowidth{#1}
  \def\@logo{#2}
}

\sheettype{Exercise}


\newcounter{exercise}
\setcounter{exercise}{1}

\newboolean{turnpagesw}
\setboolean{turnpagesw}{true}

\newboolean{showsol}
\setboolean{showsol}{false}

\renewcommand{\maketitle}{
  \begin{minipage}{0.25\textwidth}
    \begin{flushleft}\includegraphics[width=\@logowidth]{\@logo}\end{flushleft}
  \end{minipage}
  \hfill
  \begin{minipage}{0.40\textwidth}
    \begin{center}\bf\LARGE%
      \textrm{\@lecture}\\
      \LARGE\textrm{\@sheettype \, Sheet \@sheetnr}
    \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}{0.25\textwidth}
    \begin{flushright}
      \textrm{\@term}\\
      \textrm{\@lecturer} \\
    \end{flushright}
  \end{minipage}

  \begin{minipage}{0.7\textwidth}
    \ifdefined\@assistant
      \@assistant\\
    \fi
    \ifdefined\@url
      \url{\@url}
    \fi
  \end{minipage}
  \hfill
  \begin{minipage}{0.3\textwidth}
    \begin{flushright}
      \begin{tabular}{lr}
        \ifdefined\@issued
          Issued: & \@issued \\
        \fi
        \ifdefined\@due
          Due:    & \@due
        \fi
      \end{tabular}
    \end{flushright}
  \end{minipage}

  \rule{\textwidth}{1pt}
}

% use this to break a page
% creates a turn over note and creates the headline on the new page
\newcommand{\turnpage}{
  \ifthenelse{\boolean{turnpagesw}}{
    \vfill\hfill -- please turn over -- \newpage
    \makeatletter
    {\@sheettypes{} for \@lecture} \hfill Sheet \@sheetnr
    \makeatother
    \vspace{-0.2cm}\\
    \rule{\textwidth}{1pt}
  }{}}
% use this to start a new exercise
\newcommand{\exercise}[2][?]{
  \vspace{1.5em}
  \textbf{\@sheettype \theexercise:} #2 (#1 Pts.) %
  \stepcounter{exercise}
}
\makeatother

% use this to create subtaks:
\newenvironment{subtasks}{
  \newcommand{\task}{\item }
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
  \begin{enumerate}[label=\alph*)]
}
{
  \end{enumerate}
}



% set this "flag" at beginning if you want to print the solutions as well
\newcommand{\showsolutions}{
  \setboolean{showsol}{true}
  \setboolean{turnpagesw}{false} % don't create pageturns if solutions are shown
}


% mark solutions with this envirionment
\newenvironment{solution}{
  \ifthenelse{\boolean{showsol}}
  {
    \begin{framed}
    \textbf{SOLUTION:}
  }
  {\expandafter\comment}
}
{
  \ifthenelse{\boolean{showsol}}
  {
    \end{framed}
    \newpage
  }
  {\expandafter\endcomment}
}



\edef\myjob{\expandafter\scantokens\expandafter{\jobname\noexpand}}
\IfSubStringInString{sol}{\myjob}{
  \setboolean{showsol}{true}
  \setboolean{turnpagesw}{false} % don't create pageturns if solutions are shown
}{
  \def\threewords#1t#2\relax{#2}
  \immediate\write18{pdflatex -jobname=sol\expandafter\threewords\myjob\relax\space \jobname.tex}
}
